import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;

//main class, abstracts the au file into a java object to further break it down into various sub classes
//sub classes will include a data storage object, an object for the header, an object to hold changed information,
//and an object for a custom exception class to catch exceptions
public class AudioFileTestObject { 
			
	//class variables that will find a file, make a header object, and create the data structure to store the data
	 File file; 
	 AudioFileHeader audioFileHeader; 
	 ArrayList<AudioFileAlter> AudioFileObjectNew = new ArrayList<AudioFileAlter>(); 

	 //constructor that uses the file passed to it and makes the header info 
	AudioFileTestObject(File file) throws IOException , CustomException {
		this.file = file;
		audioFileHeader = AudioFileHeader.returnParsedHeader(file); 
	}

	//method that returns the necessary information for the byte array 
	 TestFileChannelInfo[] getTheInfo(int begin, int size)
			throws IOException { 
		RandomAccessFile accessedFile = new RandomAccessFile(file, "r");
		accessedFile.seek(begin);
		TestFileChannelInfo[] infoArray = TestFileChannelInfo
				.makeTheChannels(audioFileHeader.getTheType.theBitsForEachSample,
						audioFileHeader.theChannels);
		int bytesInEachSample = audioFileHeader.getTheType.theBitsForEachSample / 8;
		for (int x = 0; x < size / (bytesInEachSample * audioFileHeader.theChannels); x++) {
			for (int y = 0; y < audioFileHeader.theChannels; y++) {
				byte[] sampleByteArray = new byte[bytesInEachSample];
				accessedFile.read(sampleByteArray, 0, bytesInEachSample);
				infoArray[y].addSample(sampleByteArray);
			}
		}
		accessedFile.close();
		return infoArray;
	}

	 //writes the information to the specified file
	 void putTheInfoInFile(File file) throws IOException { 
		RandomAccessFile aNewFile = new RandomAccessFile(file, "rw");
		aNewFile.write(audioFileHeader.getTheseBytes());
		if (!file.equals(this.file)) { 
			file.delete(); 
			file.createNewFile();
			RandomAccessFile theOldFile = new RandomAccessFile(this.file, "r");
			theOldFile.seek(audioFileHeader.offset);
			int x = 0;
			while (x==0) { 
				int next = theOldFile.read();
				if (next == -1)
					x++;
				aNewFile.write(next);
			}
			theOldFile.close();
		}
		for (AudioFileAlter audioFileAlter : AudioFileObjectNew) { 
			aNewFile.seek(audioFileAlter.offset);
			int numberOfSamples = audioFileAlter.length
					/ (audioFileHeader.theChannels * (audioFileHeader.getTheType.theBitsForEachSample / 8));
			for (int x = 0; x < numberOfSamples; x++)
				for (int y = 0; y < audioFileHeader.theChannels; y++) {
					aNewFile.write(audioFileAlter.audioChannelData[y]
							.getThisInstance(x).getTheseBytes());
				}
		}
		aNewFile.close();
	}

	 //the output for the header info
	public String returnParsedHeader() {
		return audioFileHeader.displayOutput();
	} 

	//finds out how many bytes correspond to the specified seconds
	 int findBytesForSpecifiedSeconds(float seconds) {
		return (int) Math.ceil((audioFileHeader.sampling * seconds
				* audioFileHeader.getTheType.theBitsForEachSample * audioFileHeader.theChannels) / 8f);
	} 

	 //returns an altered file object to be further manipulated for fade out process
	 static AudioFileAlter doLinearFadeOut(AudioFileTestObject testFile, float seconds)
				throws IOException {
			int byteLength = testFile.findBytesForSpecifiedSeconds(seconds); 
			TestFileChannelInfo[] testFileChannels = testFile.getTheInfo(
					(int) (testFile.file.length() - byteLength), byteLength); 
			for (TestFileChannelInfo testFileChannel : testFileChannels)
				testFileChannel.doLinearFadeOut(); 
			return new AudioFileAlter((int) (testFile.file.length() - byteLength),
					byteLength, testFileChannels); 
		}

	 //returns an altered file object to be further manipulated for fade in process
		 static AudioFileAlter doLinearFadeIn(AudioFileTestObject auFile, float sec)
				throws IOException {
			int lengthBytes = auFile.findBytesForSpecifiedSeconds(sec); 
			TestFileChannelInfo[] testFileChannels = auFile.getTheInfo(auFile.audioFileHeader.offset,
					lengthBytes); 
			for (TestFileChannelInfo testFileChannel : testFileChannels)
				testFileChannel.doLinearFadeIn(); 
			return new AudioFileAlter(auFile.audioFileHeader.offset, lengthBytes,
					testFileChannels); 
		}
		 
		//takes two files and does a fade out with the first and fade in with the second
		 void doLinearCrossFade(float seconds, AudioFileTestObject output)
				throws IOException {
				
			AudioFileAlter foFile = doLinearFadeOut(this, seconds); 
			AudioFileAlter fiFile = doLinearFadeIn(output, seconds); 
			for (int x = 0; x < audioFileHeader.theChannels; x++)
				foFile.audioChannelData[x].mergeTheFiles(fiFile.audioChannelData[x]); 
			AudioFileObjectNew.add(foFile); 
			int shiftedOff = audioFileHeader.offset + audioFileHeader.totalDataSize; 
			int shiftedLen = output.audioFileHeader.totalDataSize - fiFile.length;
			AudioFileObjectNew.add(new AudioFileAlter(shiftedOff,
					shiftedLen, output.getTheInfo(output.audioFileHeader.offset
							+ fiFile.length, output.audioFileHeader.totalDataSize
							- fiFile.length)));
			audioFileHeader.totalDataSize += shiftedLen; 
		}
	//does a linear fade out of specified file object
	 void doLinearFadeOut(float seconds) throws IOException {
		AudioFileObjectNew.add(doLinearFadeOut(this, seconds));
	}

	 //does a linear fade out of the specified file object
	 void doLinearFadeIn(float seconds) throws IOException {
		AudioFileObjectNew.add(doLinearFadeIn(this, seconds));
	}

	public static AudioFileTestObject decodeFile(File file) throws IOException , CustomException {
		return new AudioFileTestObject(file);
	} 
	
	
	/*****************************************************************/
	
	
	//custom exception to catch non au files
	 static class CustomException extends Exception { 
		 static final long serialVersionUID = 1L;

		 CustomException() { 
			 super("This isn't a valid .au file. Please pick a proper one."); 
		 } 
	 }//end CustomException class
	 
	 
 /*****************************************************************/
	 
	 
	 //class that creates objects with the header info for the audio file object
	 static class AudioFileHeader { 
		 static final int auMagic = 0x2E736E64;
		 int offset, totalDataSize, sampling, theChannels;
		 byte[] commentArray = new byte[0];
		 AudioFileEncoding getTheType;
		 
		 AudioFileHeader() {
		}
		 AudioFileHeader(AudioFileEncoding getTheType, int sampling, int theChannels,
				byte[] commentArray) {
			this.totalDataSize = 0;
			this.sampling = sampling;
			this.theChannels = theChannels;
			this.getTheType = getTheType;
			this.commentArray = commentArray;
			this.offset = 24 + this.commentArray.length;
		}

		 static AudioFileHeader returnParsedHeader(File auFile) throws IOException , CustomException { 
			AudioFileHeader audioFileHeader = new AudioFileHeader();
			RandomAccessFile accessedFile = new RandomAccessFile(auFile, "r");
			if (!(auMagic == accessedFile.readInt())) {
				accessedFile.close();
				throw new CustomException();
			}
			audioFileHeader.offset = accessedFile.readInt();
			audioFileHeader.totalDataSize = accessedFile.readInt();
			audioFileHeader.getTheType = AudioFileEncoding.getEncoding(accessedFile.readInt());
			audioFileHeader.sampling = accessedFile.readInt();
			audioFileHeader.theChannels = accessedFile.readInt();
			if (audioFileHeader.offset > 24) {
				audioFileHeader.commentArray = new byte[audioFileHeader.offset - 24];
				accessedFile.read(audioFileHeader.commentArray);
			}
			accessedFile.close();
			return audioFileHeader;
		}

		 byte[] getTheseBytes() { 
			byte[] data = new byte[offset];
			ByteBuffer buffer = ByteBuffer.wrap(data);
			buffer.putInt(auMagic);
			buffer.putInt(offset);
			buffer.putInt(totalDataSize);
			buffer.putInt(getTheType.info);
			buffer.putInt(sampling);
			buffer.putInt(theChannels);
			buffer.put(commentArray);
			return data;
		}

		Object makeCopyOf() {
			return new AudioFileHeader(getTheType, sampling, theChannels,
					Arrays.copyOf(commentArray, commentArray.length));
		}

		 String displayOutput() {
			return "<html><body> <br>*****Header Info*****<br> <br>" 
					+ "Magic Number:  .snd<br>"
					+ "offset:  " + offset + "<br>"
					+ "total size of data:  " + totalDataSize + " bytes <br>"
					+ "the encoding type:  " + getTheType.displayOutput() + "<br>"
					+ "the sampling rate:  " + sampling + "<br>"
					+ "the number of channels:  " + theChannels + "<br>"
					+ "comments (if any):  " + ((commentArray.length > 0) ? new String(commentArray)
							: "no comments <br></body></html>");
		}//end AudioFileHeader class
		 
		 
		 /***************************************************************/
		 
		 
		 //class that creates objects that determine the encoding of the file; only works for 8 or 16 bit PCM
		 static class AudioFileEncoding { 
			 
			 int info, theBitsForEachSample;
			  String information;
			  
			  static AudioFileEncoding PCM_8 = new AudioFileEncoding(2, 8,
					"PCM-8bit");
			 static  AudioFileEncoding PCM_16 = new AudioFileEncoding(3, 16,
					"PCM-16bit");
			 static  AudioFileEncoding[] encodings = new AudioFileEncoding[] {
					null, null, PCM_8, PCM_16 };

			 AudioFileEncoding(int info, int theBitsForEachSample, String information) {
				this.info = info;
				this.theBitsForEachSample = theBitsForEachSample;
				this.information = information;
			}

			public boolean equals(Object o) {
				if (o.getClass().equals(Integer.TYPE))
					return (int) o == info;
				if (o instanceof AudioFileEncoding)
					return ((AudioFileEncoding) o).info == info;
				return false;
			}

			 static AudioFileEncoding getEncoding(int encoding_code) {
				return encodings[encoding_code];
			}

			 String displayOutput() {
				return info + " and " + information;
			}
		}//end AudioFileEncoding class
	}//end overall header class
	 
	 
	 /*********************************************************************/
	 
	 
	 //class that creates objects storing channel info 
	 static class TestFileChannelInfo { 
		  
		 //class variables
		 final int theBitsForEachSample;
		 ArrayList<audioFileDataStorage> data = new ArrayList<audioFileDataStorage>(); 
		 
		 TestFileChannelInfo(int theBitsForEachSample) {
			this.theBitsForEachSample = theBitsForEachSample;
		} // simple constructor

		 void addSample(byte[] sampleArray) {
				data.add(new audioFileDataStorage(theBitsForEachSample, sampleArray));
			} 
		 
		 audioFileDataStorage getThisInstance(int index) {
			return data.get(index);
		}

		 void mergeTheFiles(TestFileChannelInfo testFileChannel) {
				
				for (int x = 0; x < data.size(); x++)
					getThisInstance(x).setThisSample(
							getThisInstance(x).getThisSampleValue()
									+ testFileChannel.getThisInstance(x).getThisSampleValue()); 
			}

		 void doLinearFadeOut() {
			for (int x = 0; x < data.size(); x++)
				getThisInstance(x).doTheTransform(
						(data.size() - x) / (float) data.size());
		} 

		 void doLinearFadeIn() {
			for (int x = 0; x < data.size(); x++)
				getThisInstance(x).doTheTransform((x + 1) / (float) data.size());
		} 		 

		 static TestFileChannelInfo[] makeTheChannels(
				int theBitsForEachSample, int channelNumber) { 
			TestFileChannelInfo[] infoArray = new TestFileChannelInfo[channelNumber];
			for (int x = 0; x < channelNumber; x++)
				infoArray[x] = new TestFileChannelInfo(theBitsForEachSample);
			return infoArray;
		}
	}//end TestFileChannelInfo class

	 
	 /*************************************************************/
	 
	 
	 //class responsible for storing data sample in a byte array
	 static class audioFileDataStorage { 
		 byte[] sampleArray;
		 int theBitsForEachSample;

		 audioFileDataStorage(int theBitsForEachSample, byte[] sampleArray) {
			this.theBitsForEachSample = theBitsForEachSample;
			this.sampleArray = sampleArray;
		}

		 static byte[] putSampleInByteArray(int sample,
					int theBitsForEachSample) { 
				byte[] sampleArray = new byte[theBitsForEachSample / 8];
				for (int i = 0; i < theBitsForEachSample / 8; i++)
					sampleArray[sampleArray.length - 1 - i] = (byte) ((sample >> (8 * i)) & 0xFF);
				return sampleArray;
			}

		 byte[] getTheseBytes() {
				return sampleArray;
			} 

			 int getThisSampleValue() {
				return byteToSample(theBitsForEachSample, sampleArray);
			} 
			 
			 void doTheTransform(float x) {
					setThisSample(Math.round(getThisSampleValue() * x));
				} 

			 void setThisSample(byte[] sampleArray) {
				this.sampleArray = sampleArray;
			} 

			 void setThisSample(int sample) {
				sampleArray = putSampleInByteArray(sample, theBitsForEachSample);
			} 
			 static int byteToSample(int theBitsForEachSample,
					byte[] sampleArray) { 
				int sampleValue = 0;
				for (int x = 0; x < sampleArray.length; x++)
					sampleValue += (sampleArray[x] & 0xFF) << (8 * (sampleArray.length - 1 - x));
				if (sampleArray[0] < 0)
					sampleValue += -1 << theBitsForEachSample; 
				return sampleValue;
			}		
	}//end audioFileDataStorage class
	 
	 
	/***********************************************************************/
	 
	 
	 //class that creates objects that act as intermediate info holders while writing to file is taking place
	 static class AudioFileAlter { 
		  int offset, length;
		  TestFileChannelInfo[] audioChannelData;

		 AudioFileAlter(int offset, int length,
				TestFileChannelInfo[] audioChannelData) {
			this.offset = offset;
			this.length = length;
			this.audioChannelData = audioChannelData;
		}

		public boolean equals(Object obj) {
			if (obj.getClass().equals(Integer.TYPE))
				return (int) obj == offset;
			if (obj instanceof AudioFileAlter)
				return ((AudioFileAlter) obj).offset == offset;
			return false;
		}
	}//end AudioFileAlter class
}//end overall AudioFileTestObject class 
