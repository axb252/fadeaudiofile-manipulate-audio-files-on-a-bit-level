
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;

import java.awt.*;
import java.awt.event.*;
import java.io.File;

public class Driver {

	public static void main(String[] args) {
		// set GUI components
		FrameCreator Frame = new FrameCreator();
		Frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Frame.setVisible(true);
	}

}

class FrameCreator extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L; //make eclipse happy
	
	String input;
	int intInput;
	JTextArea log;
	JFileChooser fc;
	JLabel testLabel = new JLabel("no file yet");
	JLabel testLabel2 = new JLabel("");
	JPanel radioButtonPanel;
	JRadioButton button1, button2, button3, button4;
	ButtonGroup radioButtonGroup;
	File file;
	File file2;
	File file3;

	public FrameCreator() {
		//set GUI dimensions
		int height = 700;
		int width = 800;
		setSize(width, height);
		setLocation(0, 0);

		//open file chooser for choosing files
		fc = new JFileChooser();

		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

		setTitle("Options");

		radioButtonPanel = new JPanel();

		radioButtonGroup = new ButtonGroup();
		
		//directions
		JLabel label1 = new JLabel(
				"Please select one of the following 4 options.");
		JLabel label2 = new JLabel(
				"You will be prompted 3 times for fade in and fade out, first for input file, second for output file, and finally for the duration of the fade.");
		JLabel label3 = new JLabel(
				"For cross fade you will be prompted 4 times; first twice for the input files, third for the output file, and finally for the duration of the fade.");
		JLabel label4 = new JLabel(
				"Exit will close the program");
		
		//set buttons
		button1 = new JRadioButton("Linear Fade-in", false);
		button2 = new JRadioButton("Linear Fade-out", false);
		button3 = new JRadioButton("Linear Cross Fade", false);
		button4 = new JRadioButton("Exit Program", false);
		
		
		//set radio panel elements and orientation
		radioButtonPanel.setLayout(new BoxLayout(radioButtonPanel,
				BoxLayout.Y_AXIS));

		radioButtonGroup.add(button1);
		radioButtonGroup.add(button2);
		radioButtonGroup.add(button3);
		radioButtonGroup.add(button4);
		
		radioButtonPanel.add(label1);
		radioButtonPanel.add(label2);
		radioButtonPanel.add(label3);
		radioButtonPanel.add(label4);
		radioButtonPanel.add(button1);
		radioButtonPanel.add(button2);
		radioButtonPanel.add(button3);
		radioButtonPanel.add(button4);

		radioButtonPanel.add(testLabel);
		radioButtonPanel.add(testLabel2);

		add(radioButtonPanel, BorderLayout.CENTER);

		//action listener for fade in, will prompt user for input and output files as well as duration
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int returnVal = fc.showOpenDialog(FrameCreator.this);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					file = fc.getSelectedFile();

				} 

				int returnVal2 = fc.showSaveDialog(FrameCreator.this);
				if (returnVal2 == JFileChooser.APPROVE_OPTION) {
					file2 = fc.getSelectedFile();

				} 
				input = JOptionPane.showInputDialog(null,
						"Please enter length of fade");
				intInput = Integer.parseInt(input);
				
				try {
					AudioFileTestObject firstAudio;
					firstAudio = new AudioFileTestObject(file);
					AudioFileTestObject tempFile = firstAudio;
					tempFile = AudioFileTestObject.decodeFile(file);
					testLabel.setText(tempFile.returnParsedHeader());
					firstAudio.doLinearFadeIn(intInput);
					firstAudio.putTheInfoInFile(file2);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				repaint();
			}
		});

		//action listener for fade out, will prompt user for input and output files as well as duration
		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				
				int returnVal = fc.showOpenDialog(FrameCreator.this);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					file = fc.getSelectedFile();
				} 
				// Handle save button action.

				int returnVal2 = fc.showSaveDialog(FrameCreator.this);
				if (returnVal2 == JFileChooser.APPROVE_OPTION) {
					file2 = fc.getSelectedFile();
				} 
				input = JOptionPane.showInputDialog(null,
						"Please enter length of fade");
				intInput = Integer.parseInt(input);
				try {
					AudioFileTestObject firstAudio;
					firstAudio = new AudioFileTestObject(file);
					AudioFileTestObject tempFile = firstAudio;
					tempFile = AudioFileTestObject.decodeFile(file);
					testLabel.setText(tempFile.returnParsedHeader());
					firstAudio.doLinearFadeOut(intInput);
					firstAudio.putTheInfoInFile(file2);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			
				repaint();

			}
		});
		
		//action listener for cross fade, will prompt user for input and output files as well as duration
		button3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				
				int returnVal = fc.showOpenDialog(FrameCreator.this);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					file = fc.getSelectedFile();
				} 
				int returnVal3 = fc.showOpenDialog(FrameCreator.this);

				if (returnVal3 == JFileChooser.APPROVE_OPTION) {
					file3 = fc.getSelectedFile();
					

				} 
				// Handle save button action.

				int returnVal2 = fc.showSaveDialog(FrameCreator.this);
				if (returnVal2 == JFileChooser.APPROVE_OPTION) {
					file2 = fc.getSelectedFile();
					

				} 
				input = JOptionPane.showInputDialog(null,
						"Please enter length of fade");
				intInput = Integer.parseInt(input);

				

				try {
					AudioFileTestObject firstAudio;
					firstAudio = new AudioFileTestObject(file);
					AudioFileTestObject tempFile = firstAudio;
					tempFile = AudioFileTestObject.decodeFile(file);
					
					AudioFileTestObject secondAudio = new AudioFileTestObject(file3);
					AudioFileTestObject tempFile2 = secondAudio;
					tempFile2 = AudioFileTestObject.decodeFile(file3);
					testLabel.setText(tempFile.returnParsedHeader());
					testLabel2.setText(tempFile2.returnParsedHeader());
					firstAudio.doLinearCrossFade(intInput, secondAudio);
					firstAudio.putTheInfoInFile(file2);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				repaint();
			}
		});
		
		//action listener for exit button
		button4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
				repaint();
			}
		});
		
		

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

	}

}

